FROM ubuntu:16.04

MAINTAINER Feivur

ENV ANDROID_HOME /opt/android-sdk
#ENV ANDROID_NDK  /opt/android-ndk
#ENV ANDROID_NDK_HOME /opft/android-ndk

# Set locale
ENV LANG en_US.UTF-8
RUN apt-get clean && apt-get update && apt-get install -y locales
RUN locale-gen $LANG

COPY README.md /README.md

WORKDIR /tmp

# Installing packages
RUN apt-get install -yq --no-install-recommends \
		default-jre \
		build-essential \
		autoconf \
		git \
		curl \
		wget \
		lib32stdc++6 \
		lib32z1 \
		lib32z1-dev \
		lib32ncurses5 \
		libc6-dev \
		libgmp-dev \
		libmpc-dev \
		libmpfr-dev \
		libxslt-dev \
		libxml2-dev \
		m4 \
		ncurses-dev \
		ocaml \
		openssh-client \
		pkg-config \
		python-software-properties \
		software-properties-common \
		unzip \
		zip \
		mercurial \
		mc \
		zlib1g-dev && \
	rm -rf /var/lib/apt/lists/ && \
	apt-get clean

# Install Android SDK
# new https://dl.google.com/android/repository/commandlinetools-linux-7583922_latest.zip
# new 28: https://dl.google.com/android/repository/sdk-tools-linux-4333796.zip
# old https://dl.google.com/android/repository/sdk-tools-linux-3859397.zip
RUN wget -q -O cmdline-tools.zip https://dl.google.com/android/repository/commandlinetools-linux-9477386_latest.zip && \
	unzip -q cmdline-tools.zip && \
	rm -fr $ANDROID_HOME cmdline-tools.zip && \
	mkdir -p $ANDROID_HOME && \
	mv cmdline-tools $ANDROID_HOME/cmdline-tools


RUN	add-apt-repository -y ppa:openjdk-r/ppa && \
	apt-get update && \
	apt install -y openjdk-11-jdk

RUN rm -fr /usr/lib/jvm/java-8-openjdk-amd64 && \
	rm -fr /usr/lib/jvm/java-1.8.0-openjdk-amd64

# Export JAVA_HOME variable
ENV JAVA_HOME=/usr/lib/jvm/java-11-openjdk-amd64

RUN java --version

# Install Android components
RUN yes | $ANDROID_HOME/cmdline-tools/bin/sdkmanager --sdk_root=$ANDROID_HOME --licenses
#RUN $ANDROID_HOME/tools/bin/sdkmanager "cmdline-tools;latest"
ENV PATH $PATH:$ANDROID_HOME/cmdline-tools/latest/bin:$ANDROID_HOME/platform-tools

RUN	cd $ANDROID_HOME
RUN	echo y | $ANDROID_HOME/cmdline-tools/bin/sdkmanager --sdk_root=$ANDROID_HOME "build-tools;30.0.2" 
RUN	echo y | $ANDROID_HOME/cmdline-tools/bin/sdkmanager --sdk_root=$ANDROID_HOME "build-tools;30.0.3" 
RUN	echo y | $ANDROID_HOME/cmdline-tools/bin/sdkmanager --sdk_root=$ANDROID_HOME "build-tools;33.0.0" 
RUN	echo y | $ANDROID_HOME/cmdline-tools/bin/sdkmanager --sdk_root=$ANDROID_HOME "platforms;android-30" 
RUN	echo y | $ANDROID_HOME/cmdline-tools/bin/sdkmanager --sdk_root=$ANDROID_HOME "platforms;android-31" 
RUN	echo y | $ANDROID_HOME/cmdline-tools/bin/sdkmanager --sdk_root=$ANDROID_HOME "platforms;android-33" 
RUN	echo y | $ANDROID_HOME/cmdline-tools/bin/sdkmanager --sdk_root=$ANDROID_HOME "extras;android;m2repository" 
RUN	echo y | $ANDROID_HOME/cmdline-tools/bin/sdkmanager --sdk_root=$ANDROID_HOME "extras;google;google_play_services" 
RUN	echo y | $ANDROID_HOME/cmdline-tools/bin/sdkmanager --sdk_root=$ANDROID_HOME "extras;google;m2repository" 
RUN	echo y | $ANDROID_HOME/cmdline-tools/bin/sdkmanager --sdk_root=$ANDROID_HOME "extras;m2repository;com;android;support;constraint;constraint-layout-solver;1.0.2" 
RUN	echo y | $ANDROID_HOME/cmdline-tools/bin/sdkmanager --sdk_root=$ANDROID_HOME "extras;m2repository;com;android;support;constraint;constraint-layout;1.0.2" 
RUN	echo y | $ANDROID_HOME/cmdline-tools/bin/sdkmanager --sdk_root=$ANDROID_HOME "platform-tools"

# Install Android NDK, put it in a separate RUN to avoid travis-ci timeout in 10 minutes. 
#RUN wget -q -O android-ndk.zip http://dl.google.com/android/repository/android-ndk-r${ANDROID_NDK_VERSION}-linux-x86_64.zip && \
#	unzip -q android-ndk.zip && \
#	rm -fr $ANDROID_NDK android-ndk.zip && \
#	mv android-ndk-r${ANDROID_NDK_VERSION} $ANDROID_NDK


# Support Gradle
ENV TERM dumb
ENV JAVA_OPTS "-Xms4096m -Xmx4096m"
ENV GRADLE_OPTS "-XX:+UseG1GC -XX:MaxGCPauseMillis=1000"

# Confirms that we agreed on the Terms and Conditions of the SDK itself
# (if we didnt the build would fail, asking us to agree on those terms).
RUN mkdir "${ANDROID_HOME}/licenses" || true
RUN echo "8933bad161af4178b1185d1a37fbf41ea5269c55" > "${ANDROID_HOME}/licenses/android-sdk-license"

RUN chown -R 0777 $ANDROID_HOME
RUN chmod 777 $ANDROID_HOME
RUN touch $ANDROID_HOME/analytics.settings
RUN chmod 777 $ANDROID_HOME/analytics.settings