README

# create volume
docker volume create axxon-android-ci-gradle

# create image
docker build -t axxon-android-ci .

# get into console with local gradle 
docker run -it -v ~/.gradle:/root/.gradle -v $PWD:/project axxon-android-ci:latest /bin/bash

# get into console with gradle from volume
docker run -it -v axxon-android-ci-gradle:/root/.gradle -v $PWD:/project axxon-android-ci:latest /bin/bash

# build project
docker run -it -v axxon-android-ci-gradle:/root/.gradle -v $PWD:/project axxon-android-ci:latest /bin/bash -c "cd /project && ./gradlew assembleArmv7aAxxon2Release"
